package com.shinys.completeablefutuer.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDto {
    private String name;
    private Long prodNo;
}
