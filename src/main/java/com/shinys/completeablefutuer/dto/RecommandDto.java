package com.shinys.completeablefutuer.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class RecommandDto {
    private List<ProductDto> products;
}
