package com.shinys.completeablefutuer.service;

import com.shinys.completeablefutuer.dto.CategoryDto;
import com.shinys.completeablefutuer.dto.ProductDto;
import com.shinys.completeablefutuer.dto.RecommandDto;

import java.util.Arrays;

public class MockService {

    public ProductDto getProduct(Long productNo){

        System.out.println("getProduct()");

        try{
            Thread.sleep(1000L);
        }catch (Exception e){}

        return ProductDto.builder()
                .prodNo(productNo)
                .name("상품 "+productNo)
                .build();

    }


    public RecommandDto getRcommand(){

        System.out.println("getRecommand()");
        try{
            Thread.sleep(2500L);
        }catch (Exception e){}

        return RecommandDto.builder().products(

                Arrays.asList(
                        ProductDto.builder().prodNo(2L).name("아이오페").build(),
                        ProductDto.builder().prodNo(4L).name("헤라").build(),
                        ProductDto.builder().prodNo(7L).name("설화수").build(),
                        ProductDto.builder().prodNo(8L).name("이니스프리").build(),
                        ProductDto.builder().prodNo(9L).name("마스크").build()
                )
        ).build();
    }



    public CategoryDto getCategory(){
        System.out.println("getCategory()");
        try{
            Thread.sleep(2000L);
        }catch (Exception e){}
        return CategoryDto.builder().code(3L).name("상품 목록").build();
    }
}
