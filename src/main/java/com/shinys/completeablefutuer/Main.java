package com.shinys.completeablefutuer;

import com.shinys.completeablefutuer.dto.CategoryDto;
import com.shinys.completeablefutuer.dto.ProductDto;
import com.shinys.completeablefutuer.dto.RecommandDto;
import com.shinys.completeablefutuer.service.MockService;
import org.apache.commons.lang.time.StopWatch;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Main {


    private static StopWatch stopWatch = new StopWatch();

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Main main = new Main();

        MockService service = new MockService();

        main.combine1(service);

        System.out.println("\n");

        main.combine2(service);
    }


    // 현재 방식. 각 API 호출을 순차적으로 처리하고 있음
    public void combine1(MockService service){
        stopWatch.reset();
        System.out.println("======= combine1 조회 시작 ====");
        stopWatch.start();

        service.getCategory();

        service.getProduct(4L);

        service.getRcommand();

        stopWatch.stop();
        System.out.println("======= combine1 조회 끝 ====");
        System.out.println("combine1 수행시간 "+stopWatch.toString());
    }



    // 개선 제안. 각 API를 병렬로 호출하여 한번에 취합.
    // @see https://www.baeldung.com/java-completablefuture#Multiple
    public void combine2(MockService service) throws ExecutionException, InterruptedException {
        stopWatch.reset();
        System.out.println("======= combine2 조회 시작 ====");
        stopWatch.start();

        CompletableFuture<CategoryDto> categoryDtoCompletableFuture = CompletableFuture.supplyAsync(
                () -> service.getCategory(),
                executor
        );

        CompletableFuture<ProductDto> productDtoCompletableFuture = CompletableFuture.supplyAsync(
                () -> service.getProduct(4L),
                executor
        );

        CompletableFuture<RecommandDto> recommandDtoCompletableFuture = CompletableFuture.supplyAsync(
                () -> service.getRcommand(),
                executor
        );

        // future를 수행. 허용 thread pool 안에서 병렬 진행.
        // .get() 은 null 리턴.
        CompletableFuture.allOf(
                categoryDtoCompletableFuture,
                productDtoCompletableFuture,
                recommandDtoCompletableFuture
        ).get();


        stopWatch.stop();
        System.out.println("======= combine2 조회 끝 ====");
        System.out.println("combine2 수행시간 "+stopWatch.toString());
    }



    private final Executor executor = Executors.newFixedThreadPool(
            10,
            (Runnable r) -> {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
            );

}
